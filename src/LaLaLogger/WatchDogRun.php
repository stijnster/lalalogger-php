<?php

namespace LaLaLogger;

class WatchDogRun {

	const MODE_MANUAL = 0;
	const MODE_NORMAL = 1;
	const MODE_CRITICAL = 2;

	private $_identifier;
	private $_mode;

	public function __construct(string $identifier, array $options = []){
		$settings = array_merge([
			'mode' => static::MODE_NORMAL,
			'label' => time()
		], $options);

		$this->_identifier = $identifier;
		$this->_mode = (int)$settings['mode'];
		$this->_label = (string)$settings['label'];
	}

	public function getIdentifier() : string {
		return $this->_identifier;
	}

	public function getMode() : int {
		return $this->_mode;
	}

	public function getLabel() : string {
		return $this->_label;
	}

	public function start(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('start', $message, $objects);
	}

	public function end(?string $message = NULL, ?array $objects = NULL) {
		$this->stop($message, $objects);
	}

	public function stop(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('stop', $message, $objects);
	}

	public function debug(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('debug', $message, $objects);
	}

	public function info(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('info', $message, $objects);
	}

	public function notice(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('notice', $message, $objects);
	}

	public function warning(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('warning', $message, $objects);
	}

	public function error(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('error', $message, $objects);
	}

	public function critical(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('critical', $message, $objects);
	}

	public function alert(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('alert', $message, $objects);
	}

	public function emergency(?string $message = NULL, ?array $objects = NULL) {
		$this->_log('emergency', $message, $objects);
	}

	private function _log(string $level, ?string $message = NULL, ?array $objects = NULL) {
		$entry = [
				'_wr' => [
					'_w' => $this->_identifier,
					'_l' => $this->_label,
					'_esl' => [
						'_l' => $level,
						'_r' => time()
					]
				]
			];

		if($message !== NULL){
			$entry['_wr']['_esl']['_m'] = $message;
		}
		if($objects !== NULL){
			$entry['_wr']['_esl']['_o'] = $objects;
		}

		PayloadBuffer::push($entry);

		switch($this->_mode){
			case static::MODE_NORMAL:
				if(!in_array($level, [ 'debug', 'info' ]) || (PayloadBuffer::size() > 10)){
					PayloadBuffer::flush();
				}
				break;
			case static::MODE_CRITICAL:
				PayloadBuffer::flush();
				break;
		}
	}

}
