<?php

namespace LaLaLogger;

class Config {

	private static $_defaultConfig = [
		'baseURL' => 'https://api.lalalogger.com',
		'environment' => NULL,
		'curl.options' => NULL
	];

	private static $_currentConfig;

	public static function restoreDefaultConfig(){
		static::$_currentConfig = static::$_defaultConfig;
	}

	public static function getCurrentConfig(){
		if(static::$_currentConfig === NULL){
			static::restoreDefaultConfig();
		}

		return static::$_currentConfig;
	}

	public static function getKeys() : array {
		return array_keys(static::getCurrentConfig());
	}

	public static function merge(array $config) {
		static::$_currentConfig = array_merge(static::getCurrentConfig(), $config);
	}

	public static function get(string $key) {
		if(array_key_exists($key, static::getCurrentConfig())){
			return static::getCurrentConfig()[$key];
		}

		return NULL;
	}

	public static function set(string $key, $value) {
		static::merge([ $key => $value ]);
	}

}