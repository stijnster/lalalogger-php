<?php

namespace LaLaLogger;

class PayloadBuffer {

	private static $_buffer = [];
	private static $_apiClient;

	public static function getApiClient() : \LaLaLogger\ApiClient\AbstractClient {
		if(static::$_apiClient === NULL){
			static::$_apiClient = new \LaLaLogger\ApiClient\LiveClient();
		}

		return static::$_apiClient;
	}

	public static function setApiClient(?\LaLaLogger\ApiClient\AbstractClient $apiClient) {
		static::$_apiClient = $apiClient;
	}

	public static function clear() {
		static::$_buffer = [];
	}

	public static function size() : int {
		return count(static::$_buffer);
	}

	public static function getBuffer() : array {
		return static::$_buffer;
	}

	public static function push($value) {
		array_push(static::$_buffer, $value);
	}

	public static function flush(){
		if(static::size() > 0){
			list($code, $response) = static::getApiClient()->request('POST', '/api/v1/payloads', static::$_buffer);

			if($code === 201){
				static::clear();
			}
		}
	}

}