<?php

namespace LaLaLogger\ApiClient;

class TestClient extends AbstractClient {

	private $_requests = [];

	public function reset(){
		$this->_requests = [];
	}

	public function getRequests() : array {
		return $this->_requests;
	}

	public function getRequestsCount() : int {
		return count($this->_requests);
	}

	public function getLastRequest() : ?array {
		if(count($this->_requests) > 0){
			return $this->_requests[count($this->_requests) - 1];
		}

		return NULL;
	}

	public function request($method, $path, array $params = [], array $headers = [], array $options = []) : array {
		$settings = array_merge([
			'headers' => [
				'Authorization' => "Bearer {$this->_environment}",
				'Content-Type' => 'application/json',
				'Accept' => 'application/json'
			]
		], $options);
		$headers = array_merge($settings['headers'], $headers);

		$url = $this->buildURL($path);

		$request = [
			'method' => $method,
			'url' => $url,
			'params' => $params,
			'headers' => $this->compileHeaders($headers),
		];

		array_push($this->_requests, $request);

		if($method === 'POST'){
			return [ 201, NULL ];
		}

		return [ 404, NULL ];
	}

}
