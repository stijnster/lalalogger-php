<?php

namespace LaLaLogger\ApiClient;

abstract class AbstractClient {

	private $_baseURL;

	public function __construct(){
		$this->_baseURL = \LaLaLogger\Config::get('baseURL');
		$this->_environment = \LaLaLogger\Config::get('environment');
		$this->_curlOptions = \LaLaLogger\Config::get('curl.options');
	}

	public function getBaseURL() : ?string {
		return $this->_baseURL;
	}

	public function getEnvironment() : ?string {
		return $this->_environment;
	}

	public function getCurlOptions() {
		return $this->_curlOptions;
	}

	abstract public function request(string $method, string $path, array $params = [], array $headers = [], array $options = []) : array;

	public function buildURL(string $path){
		return rtrim($this->_baseURL, '/').$path;
	}

	public function compileHeaders(array $headers) : array {
		return array_map(function($key, $value){
			return "$key: $value";
		}, array_keys($headers), $headers);
	}

}