<?php

namespace LaLaLogger\ApiClient;

class LiveClient extends AbstractClient {

	public function request($method, $path, array $params = [], array $headers = [], array $options = []) : array {
		$settings = array_merge([
			'headers' => [
				'Authorization' => "Bearer {$this->_environment}",
				'Content-Type' => 'application/json',
				'Accept' => 'application/json'
			]
		], $options);

		$headers = array_merge($settings['headers'], $headers);

		$url = $this->buildURL($path);

		if($method === 'GET'){
			if(count($params) > 0){
				$url .= http_build_query($params);
			}
		}

		if($headers['Content-Type'] === 'application/json'){
			$params = json_encode($params);
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->compileHeaders($headers));

		if($method !== 'GET'){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		}

		if(isset($this->_curlOptions) && is_array($this->_curlOptions)){
			foreach($this->_curlOptions as $key => $value){
				curl_setopt($curl, $key, $value);
			}
		}

		$response = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		if(($code >= 200) && ($code < 300)){
			if($headers['Accept'] === 'application/json'){
				return [ $code, json_decode($response, true) ];
			}
		}

		return [ $code, $response ];
	}

}
