<?php

require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap.php');

class DevServerTest extends \PHPUnit\Framework\TestCase{

	public function setUp() : void {
		\LaLaLogger\Config::restoreDefaultConfig();
		\LaLaLogger\Config::set('baseURL', 'http://localhost:3000');
		\LaLaLogger\Config::set('environment', 'PgKPDgz3cLKoIPIsXnuzazY0NPtoASIhO0qKRDSqjH');
		\LaLaLogger\PayloadBuffer::setApiClient(NULL);
		\LaLaLogger\PayloadBuffer::clear();
	}

	public function testPayloads(){
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
		$run = new \LaLaLogger\WatchDogRun(pathinfo(__FILE__, PATHINFO_FILENAME));
		$run->start();
		$run->debug('Hello World');
		$run->info('This is me!');
		$run->end();
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
	}

}
