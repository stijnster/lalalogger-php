<?php

require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap.php');

class LaLaLoggerWatchDogRunTest extends \PHPUnit\Framework\TestCase{

	public function testDefaultSettings() {
		$run = new \LaLaLogger\WatchDogRun(pathinfo(__FILE__, PATHINFO_FILENAME));
		$this->assertEquals('WatchDogRunTest', $run->getIdentifier());
		$this->assertEquals(\LaLaLogger\WatchDogRun::MODE_NORMAL, $run->getMode());
		$this->assertEquals(time(), $run->getLabel());
	}

	public function testWatchDogRun() {
		\LaLaLogger\PayloadBuffer::clear();

		$run = new \LaLaLogger\WatchDogRun(pathinfo(__FILE__, PATHINFO_FILENAME), [
			'mode' => \LaLaLogger\WatchDogRun::MODE_MANUAL
		]);

		$this->assertEquals('WatchDogRunTest', $run->getIdentifier());
		$this->assertEquals(\LaLaLogger\WatchDogRun::MODE_MANUAL, $run->getMode());
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());

		$startTime = time();
		$run->start();
		sleep(1);
		$debugTime = time();
		$run->debug('Hello World');
		sleep(1);
		$endTime = time();
		$run->end();
		sleep(1);

		$this->assertEquals(3, \LaLaLogger\PayloadBuffer::size());
		$buffer = \LaLaLogger\PayloadBuffer::getBuffer();

		$this->assertEquals([ '_wr' ], array_keys($buffer[0]));
		$this->assertEquals([ '_w', '_l', '_esl' ], array_keys($buffer[0]['_wr']));
		$this->assertEquals('WatchDogRunTest', $buffer[0]['_wr']['_w']);
		$this->assertEquals($run->getLabel(), $buffer[0]['_wr']['_l']);
		$this->assertEquals([ '_l' => 'start', '_r' => $startTime ], $buffer[0]['_wr']['_esl']);

		$this->assertEquals('WatchDogRunTest', $buffer[1]['_wr']['_w']);
		$this->assertEquals($run->getLabel(), $buffer[1]['_wr']['_l']);
		$this->assertEquals([ '_l' => 'debug', '_r' => $debugTime, '_m' => 'Hello World' ], $buffer[1]['_wr']['_esl']);

		$this->assertEquals('WatchDogRunTest', $buffer[2]['_wr']['_w']);
		$this->assertEquals($run->getLabel(), $buffer[2]['_wr']['_l']);
		$this->assertEquals([ '_l' => 'stop', '_r' => $endTime ], $buffer[2]['_wr']['_esl']);
	}

	public function testWatchDogRunWithTestClient() {
		\LaLaLogger\Config::restoreDefaultConfig();
		\LaLaLogger\Config::set('environment', 'test1234');

		$apiClient = new \LaLaLogger\ApiClient\TestClient();

		\LaLaLogger\PayloadBuffer::clear();
		\LaLaLogger\PayloadBuffer::setApiClient($apiClient);

		$run = new \LaLaLogger\WatchDogRun(pathinfo(__FILE__, PATHINFO_FILENAME));
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
		$this->assertEquals(0, $apiClient->getRequestsCount());
		$run->start();
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
		$this->assertEquals(1, $apiClient->getRequestsCount());

		$request = $apiClient->getLastRequest();
		$this->assertNotNull($request);
		$this->assertEquals('POST', $request['method']);
		$this->assertEquals('https://api.lalalogger.com/api/v1/payloads', $request['url']);
		$this->assertEquals([ 'Authorization: Bearer test1234', 'Content-Type: application/json', 'Accept: application/json' ], $request['headers']);
		$this->assertEquals(1, count($request['params']));
		$this->assertEquals([ '_wr' ], array_keys($request['params'][0]));
		$this->assertEquals('start', $request['params'][0]['_wr']['_esl']['_l']);

		$run->debug('Hello World');
		$run->info('This is me!');
		$this->assertEquals(2, \LaLaLogger\PayloadBuffer::size());
		$this->assertEquals(1, $apiClient->getRequestsCount());
		\LaLaLogger\PayloadBuffer::flush();

		$request = $apiClient->getLastRequest();
		$this->assertNotNull($request);
		$this->assertEquals(2, count($request['params']));
		foreach($request['params'] as $params){
			$this->assertEquals([ '_wr' ], array_keys($params));
		}

		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
		$this->assertEquals(2, $apiClient->getRequestsCount());
		$run->end();
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
		$this->assertEquals(3, $apiClient->getRequestsCount());

		$request = $apiClient->getLastRequest();
		$this->assertNotNull($request);
		$this->assertEquals(1, count($request['params']));
		$this->assertEquals([ '_wr' ], array_keys($request['params'][0]));
		$this->assertEquals('stop', $request['params'][0]['_wr']['_esl']['_l']);
	}

}
