<?php

require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap.php');

class LaLaLoggerPayloadBufferTest extends \PHPUnit\Framework\TestCase{

	public function testBuffer(){
		\LaLaLogger\PayloadBuffer::clear();
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());

		\LaLaLogger\PayloadBuffer::push([ 'some' => 'value' ]);
		\LaLaLogger\PayloadBuffer::push([ 'another' => 'value' ]);
		$this->assertEquals([ [ 'some' => 'value' ], [ 'another' => 'value' ] ], \LaLaLogger\PayloadBuffer::getBuffer());
		$this->assertEquals(2, \LaLaLogger\PayloadBuffer::size());

		\LaLaLogger\PayloadBuffer::clear();
		$this->assertEquals(0, \LaLaLogger\PayloadBuffer::size());
	}

}
