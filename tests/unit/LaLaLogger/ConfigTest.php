<?php

require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap.php');

class LaLaLoggerConfigTest extends \PHPUnit\Framework\TestCase{

	public function testDefaultConfig(){
		\LaLaLogger\Config::restoreDefaultConfig();

		$this->assertEquals('https://api.lalalogger.com', \LaLaLogger\Config::get('baseURL'));
		$this->assertNull(\LaLaLogger\Config::get('curl.options'));

		$this->assertEquals([ 'baseURL', 'environment', 'curl.options' ], \LaLaLogger\Config::getKeys());
	}

	public function testRestoreDefaultConfig(){
		\LaLaLogger\Config::set('baseURL', 'http://localhost:3000');
		\LaLaLogger\Config::set('curl.options', [ 'some' => 'curl', 'options' => 123 ]);

		$this->assertEquals('http://localhost:3000', \LaLaLogger\Config::get('baseURL'));
		$this->assertEquals([ 'some' => 'curl', 'options' => 123 ], \LaLaLogger\Config::get('curl.options'));

		\LaLaLogger\Config::restoreDefaultConfig();

		$this->assertEquals('https://api.lalalogger.com', \LaLaLogger\Config::get('baseURL'));
		$this->assertNull(\LaLaLogger\Config::get('curl.options'));
	}

	public function testMergeConfig(){
		\LaLaLogger\Config::restoreDefaultConfig();

		\LaLaLogger\Config::merge([
			'baseURL' => 'http://localhost:4000',
			'some' => 'value'
		]);

		$this->assertEquals('http://localhost:4000', \LaLaLogger\Config::get('baseURL'));
		$this->assertNull(\LaLaLogger\Config::get('curl.options'));
		$this->assertEquals('value', \LaLaLogger\Config::get('some'));
	}

}
